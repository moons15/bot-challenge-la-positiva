__author__ = 'richard'
from fabric.api import *
from fabric.colors import green

env.user = 'ubuntu'
env.host_string = '167.99.96.73'
env.password = '992424558'
home_path = "/home/ubuntu/projects"
settings_staging = "--settings='corebackend.settings.staging'"
activate_env_staging = "source {}/envs/lapositivaenv/bin/activate".format(
    home_path)
manage = "python manage.py"


def deploy_staging():
    print("Beginning Deploy:")
    with cd("{}/bot-challenge-la-positiva".format(home_path)):
        run("git pull")
        run("{} && {} collectstatic --noinput {}".format(activate_env_staging,
                                                         manage,
                                                         settings_staging))
        run("{} && {} migrate {}".format(activate_env_staging, manage,
                                         settings_staging))
        sudo("service nginx restart", pty=False)
        sudo("supervisorctl restart gunicorn_bot-challenge-la-positiva", pty=False)

    print(green("Deploy bot-challenge successful"))


def createsuperuser_staging():
    with cd("{}/bot-challenge-la-positiva".format(home_path)):
        run("{} && {} createsuperuser {}".format(activate_env_staging, manage,
                                                 settings_staging))
    print(green("Createsuperuser successful"))
