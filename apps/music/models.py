from django.db.models import *
from django.db import models
from django.contrib.auth.models import PermissionsMixin, BaseUserManager, \
    AbstractBaseUser
from enum import Enum


class Song(models.Model):
    track_id = models.CharField(unique=True, max_length=100)
    track_name = models.CharField(max_length=200)
    artist_name = models.CharField(max_length=200)
    album_name = models.CharField(max_length=200)

    is_active = models.BooleanField(default=True)
    created_at = models.DateField(auto_now_add=True)
    update_at = models.DateField(auto_now=True)

    def __str__(self):
        return self.track_name

    class Meta:
        verbose_name = "Song"
        verbose_name_plural = "Songs"
