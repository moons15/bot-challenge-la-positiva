# Generated by Django 2.2.3 on 2019-07-19 00:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accountBot', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserLeadBot',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fb_id', models.CharField(max_length=100, unique=True)),
                ('first_name', models.CharField(max_length=200)),
                ('last_name', models.CharField(max_length=200)),
                ('profile_pic', models.TextField()),
                ('is_active', models.BooleanField(default=True)),
                ('created_at', models.DateField(auto_now_add=True)),
                ('update_at', models.DateField(auto_now=True)),
            ],
            options={
                'verbose_name': 'User Lead Bot',
                'verbose_name_plural': 'Users Leads Bot',
            },
        ),
    ]
