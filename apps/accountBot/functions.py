from django.conf import settings
import json, requests, random, re
from pprint import pprint
from apps.accountBot.models import UserLeadBot
from apps.music.models import Song
from datetime import datetime
from corebackend.utils.musixmatch_base import music_query_search

welcome = {
    'hello':
        [
            """Hello, I'm LabBot bot and I can recognize any song that you prefer , just send me a part of the lyrics""",
            """Hey hey hey, I'm your favourite bot called LabBot and I can recognize any song that you prefer , just send me a part of the lyrics"""]
}


# TODO = Helper functions

def get_song(message):
    tracks = music_query_search(message)
    start_send_message = 'We found {} songs, which are : \n '.format(
        len(tracks))
    medium_send_message = ''
    for track in tracks:
        try:
            song = Song.objects.get(track_id=track['track']['track_id'])
        except:
            song = Song.objects.create(
                track_id=track['track']['track_id'],
                track_name=track['track']['track_name'],
                artist_name=track['track']['artist_name'],
                album_name=track['track']['album_name'],
            )
        medium_send_message = \
            medium_send_message + \
            'id: {} \n name : {} \n artist_name: {} \n \n'.format(
                song.track_id, song.track_name, song.artist_name)

    end_send_message = 'If u wanna save your favourite song, please, send us the song id.'
    text_music = start_send_message + medium_send_message + end_send_message
    if len(tracks) == 0:
        text_music = "I'm sorry, we haven't found song"
    return text_music


def save_conversation(user, message):
    array_json_conversation = []
    json_conversation = {
        "sender_id": message['sender']['id'],
        "timestamp": message['timestamp'],
        "text": message['message']['text'],
        "datetime": str(datetime.today())
    }
    if user.conversations:
        user.conversations.append(json_conversation)
    else:
        array_json_conversation.append(json_conversation)
        user.conversations = array_json_conversation
    user.save()


def post_facebook_message(fbid, recevied_message, message):
    user_details_url = "https://graph.facebook.com/v2.6/%s" % fbid
    user_details_params = {'fields': 'first_name,last_name,profile_pic',
                           'access_token': settings.PAGE_ACCESS_TOKEN}
    user_details = requests.get(user_details_url, user_details_params).json()

    try:
        user = UserLeadBot.objects.get(fb_id=fbid)
    except:
        user = UserLeadBot.objects.create(
            fb_id=fbid, first_name=user_details['first_name'],
            last_name=user_details['last_name'],
            profile_pic=user_details['profile_pic']
        )
    save_conversation(user, message)

    words = re.sub(r"[^a-zA-Z0-9\s]", ' ', recevied_message).lower().split()

    new_text = ''
    for word in words:
        if word in welcome:
            # welcome_text = jokes[word][1]
            new_text = random.choice(welcome[word])
            break
        if Song.objects.filter(track_id=word):
            song = Song.objects.get(track_id=word)
            user.favourite_songs.add(song)
            user.save()
            new_text = "{} it' s your new favourite song".format(
                song.track_name)
            break

    if not new_text:
        text_song = get_song(recevied_message)
        new_text = 'Hey ' + user.first_name + '! ' + text_song

    post_message_url = 'https://graph.facebook.com/v2.6/me/messages?access_token=%s' % settings.PAGE_ACCESS_TOKEN
    response_msg = json.dumps(
        {"recipient": {"id": user.fb_id}, "message": {"text": new_text}})
    requests.post(post_message_url,
                  headers={"Content-Type": "application/json"},
                  data=response_msg)
