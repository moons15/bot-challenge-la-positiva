from django.contrib import admin
from .models import *
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Group
from rest_framework.authtoken.admin import Token


class User_Admin(UserAdmin):
    fieldsets = (
        (None, {'fields': (
            'email', 'password', 'full_name', 'is_superuser')}),
        (('Important dates'),
         {'fields': ('last_login', 'is_active')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )
    list_display = ['id', 'email', 'full_name', 'is_active', 'created_at']
    list_filter = ('is_active',)
    search_fields = ('full_name', 'email',)
    ordering = ('email',)
    filter_horizontal = ('groups', 'user_permissions',)


admin.site.register(User, User_Admin)


class UserLeadBotAdmin(admin.ModelAdmin):
    model = UserLeadBot
    list_display = ['fb_id', 'first_name', 'last_name', 'is_active',
                    'created_at']


admin.site.register(UserLeadBot, UserLeadBotAdmin)
admin.site.unregister(Group)
admin.site.unregister(Token)
