import json, requests, random, re
from pprint import pprint

from django.views import generic
from rest_framework import generics, status
from django.http.response import HttpResponse
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from apps.accountBot.api.serializers import BotStatsSerializer, UserSerializer
from apps.accountBot.models import UserLeadBot

from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.conf import settings
from apps.accountBot.functions import post_facebook_message


class BotSyncMessengerView(generic.View):
    def get(self, request, *args, **kwargs):
        if self.request.GET['hub.verify_token'] == settings.VERIFY_TOKEN:
            return HttpResponse(self.request.GET['hub.challenge'])
        else:
            return HttpResponse('Error, invalid token')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return generic.View.dispatch(self, request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        incoming_message = json.loads(self.request.body.decode('utf-8'))
        for entry in incoming_message['entry']:
            for message in entry['messaging']:
                if 'message' in message:
                    post_facebook_message(message['sender']['id'],
                                          message['message']['text'],
                                          message)
        return HttpResponse()


class BotStatsView(generics.GenericAPIView):
    permission_classes = AllowAny,
    serializer_class = BotStatsSerializer

    def get(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data,
                                         context={"request": request})
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class UserAPIView(generics.RetrieveAPIView):
    permission_classes = AllowAny,
    serializer_class = UserSerializer

    def get_object(self):
        id = self.kwargs.get('user_id')
        user = generics.get_object_or_404(UserLeadBot.objects.all(), id=id)
        return user
