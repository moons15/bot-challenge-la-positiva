from django.conf.urls import url
from apps.accountBot.api.views import BotSyncMessengerView, BotStatsView, \
    UserAPIView

app_name = 'accountBot-bot'
urlpatterns = [
    url(r'^66d2b8f4a09cd35cb23076a1da5d51529136a3373fd570b122/?$',
        BotSyncMessengerView.as_view()),

    url(r'^stats/?$', BotStatsView.as_view()),
    url(r'^user/(?P<user_id>\w+)$', UserAPIView.as_view())
]
