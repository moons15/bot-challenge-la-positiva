from rest_framework import serializers
from apps.accountBot.models import UserLeadBot
from apps.music.models import Song
from datetime import datetime


class UserSerializer(serializers.ModelSerializer):
    favourite_songs = serializers.StringRelatedField(many=True)

    class Meta:
        model = UserLeadBot
        exclude = ('id', 'is_active', 'created_at', 'update_at')


class SongSerializer(serializers.ModelSerializer):
    class Meta:
        model = Song
        exclude = ('is_active', 'created_at', 'update_at')


class BotStatsSerializer(serializers.ModelSerializer):
    people_use = serializers.SerializerMethodField()
    chats_per_day = serializers.SerializerMethodField()
    most_popular_song = serializers.SerializerMethodField()
    average_session_time = serializers.SerializerMethodField()

    class Meta:
        model = UserLeadBot
        fields = ('people_use', 'chats_per_day', 'most_popular_song',
                  'average_session_time')

    def get_people_use(self, obj):
        people_use = UserLeadBot.objects.filter(is_active=True).count()
        return people_use

    def get_chats_per_day(self, obj):
        chats_per_day = 0
        users = UserLeadBot.objects.filter(is_active=True)
        for user in users:
            for user_body in user.conversations:
                date = datetime.strptime(user_body['datetime'].split('.')[0],
                                         "%Y-%m-%d %H:%M:%S")
                if date.strftime("%Y-%m-%d") == datetime.today().strftime(
                        "%Y-%m-%d"):
                    chats_per_day = chats_per_day + 1
        return chats_per_day

    def get_most_popular_song(self, obj):
        songs = Song.objects.filter(is_active=True).order_by(
            'users__favourite_songs')
        try:
            song = songs.first()
        except:
            song = None
        return SongSerializer(song).data

    def get_average_session_time(self, obj):
        max_date = datetime.strptime("2010-12-30 01:00:00", "%Y-%m-%d %H:%M:%S")
        min_date = datetime.strptime("2020-12-30 23:59:59", "%Y-%m-%d %H:%M:%S")
        users = UserLeadBot.objects.filter(is_active=True)
        for user in users:
            for user_body in user.conversations:
                date = datetime.strptime(user_body['datetime'].split('.')[0],
                                         "%Y-%m-%d %H:%M:%S")
                if date.strftime("%Y-%m-%d") == datetime.today().strftime(
                        "%Y-%m-%d"):
                    try:
                        if date.strftime("%H:%M:%S") < min_date.strftime(
                                "%H:%M:%S"):
                            min_date = date
                        if date.strftime("%H:%M:%S") >= max_date.strftime(
                                "%H:%M:%S"):
                            max_date = date
                    except:
                        pass
        average_session_time = max_date - min_date
        return average_session_time
