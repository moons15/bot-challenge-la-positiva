from django.conf import settings
import json, requests, random, re
from apps.music.models import Song


def music_query_search(message):
    # musixmatch api base url
    base_url = "https://api.musixmatch.com/ws/1.1/"

    # api key
    api_key = "&apikey={}".format(settings.API_KEY_MUSIX_MATCH)

    # api method
    track_search = "track.search"

    # format url
    format_url = "?format=json&callback=callback"

    # query params

    q1 = word_in_lyrics_parameter = "&q_lyrics="
    q2 = quorum_factor_parameter = "&quorum_factor=1"

    query = base_url + track_search + format_url + q1 + message + q2 + api_key
    status = requests.get(query, headers={"Content-Type": "application/json"})
    tracks = status.json()['message']['body']['track_list']

    return tracks
